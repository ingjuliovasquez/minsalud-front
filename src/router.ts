import { createRouter, createWebHistory } from "vue-router";
import DashboardLayout from "./layouts/DashboardLayout.vue";

export default createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/",
            component: DashboardLayout,
            children: [
                {
                    path: "",
                    component: () => import("./pages/Index.vue"),
                },
            ],
        },
    ],
});
