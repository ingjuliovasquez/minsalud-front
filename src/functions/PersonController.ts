import axios, { AxiosResponse } from 'axios';
import { Persona } from '../interfaces/Persona';

const baseURL = 'http://localhost:5148';
const dir = "/api/Personas";



export const PersonController = {
  getAllPersons: async (): Promise<Persona[]> => {
    const response: AxiosResponse<Persona[]> = await axios.get(`${baseURL}${dir}`);
    return response.data;
  },

  getPersonById: async (id: number): Promise<Persona | null> => {
    try {
      const response: AxiosResponse<Persona> = await axios.get(`${baseURL}${dir}/${id}`);
      return response.data;
    } catch (error:any) {
      if (error.response && error.response.status === 404) {
        return null; // Person not found
      }
      throw error;
    }
  },

  createPerson: async (newPerson: Persona): Promise<void> => {
    await axios.post(`${baseURL}${dir}`, newPerson);
  },

  updatePerson: async (id: number, updatedPerson: Persona): Promise<void> => {
    await axios.put(`${baseURL}${dir}/${id}`, updatedPerson);
  },

  deletePerson: async (id: number): Promise<void> => {
    await axios.delete(`${baseURL}${dir}/${id}`);
  },
};
