import axios, { AxiosResponse } from 'axios';
import { Seguimiento } from '../interfaces/Seguimiento';

const baseURL = 'http://localhost:5148';
const dir = "/api/Seguimientos";



export const SeguimientoController = {
  getAllSeguimientos: async (id:number): Promise<Seguimiento[]> => {
    const response: AxiosResponse<Seguimiento[]> = await axios.get(`${baseURL}${dir}/${id}`);
    return response.data;
  },

  getSeguimientoById: async (id: number, idPersona: number): Promise<Seguimiento | null> => {
    try {
      const response: AxiosResponse<Seguimiento> = await axios.get(`${baseURL}${dir}?id=${id}&idPersona=${idPersona}`);
      return response.data;
    } catch (error:any) {
      if (error.response && error.response.status === 404) {
        return null;
      }
      throw error;
    }
  },

  createSeguimiento: async (newSeguimiento: Seguimiento): Promise<void> => {
    await axios.post(`${baseURL}${dir}`, newSeguimiento);
  },

  updateSeguimiento: async (id: number, updatedSeguimiento: Seguimiento): Promise<void> => {
    await axios.put(`${baseURL}${dir}/${id}`, updatedSeguimiento);
  },

  deleteSeguimiento: async (id: number): Promise<void> => {
    await axios.delete(`${baseURL}${dir}/${id}`);
  },
};
