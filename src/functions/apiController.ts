import axios, { AxiosResponse } from 'axios'

interface Response{
    items: any[]
}



export const apiController = {
    getData: async (dir:String): Promise<object> => {

      
        const response: AxiosResponse<Response> = await axios.get(`https://web.sispro.gov.co/directoriogeneral/api/${dir}`)
        return response.data.items
      },
}