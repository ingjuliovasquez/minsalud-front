export interface Persona {
    id: number,
    tipoIdentificacion: string,
    nroIdentificacion: string,
    primerNombre: string,
    segundoNombre: string,
    primerApellido: string,
    segundoApellido: string,
    sexo: string,
    fechaNacimiento: Date,
    codMpioResidencia: string,
    codAsegurador: string,
    created: Date | null,
    updated: Date | null
}