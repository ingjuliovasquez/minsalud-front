export interface Seguimiento {
    id: number,
    idPersona: number,
    estadoVital: 0 | 1, //0 = vivo, 1 = fallecido
    fechaDefuncion: Date | null,
    ubicacionDefuncion: 0 | 1 | null, //0 = IPS, 1 = Hogar
    codLugarAtencion: string,
    fechaAtencion: Date,
    pesoKg: number,
    tallaCm: number,
    codClasificacionNutricional: string,
    codManejoActual: string,
    desManejo: string,
    codUbicacion: string,
    desUbicacion: string,
    codTratamiento: string,
    totalSobresFTLC: number,
    otroTratamiento: string,
    created: Date,
    updated: Date
}